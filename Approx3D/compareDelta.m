function [scoreRDC1, scoreRDC2] = compareDelta(Tensor1,Tensor2,EstTensor1,EstTensor2,vecs)
rdc1 = generateRDCs(vecs,Tensor1);
rdc2 = generateRDCs(vecs,Tensor2);
e_rdc1 = generateRDCs(vecs,EstTensor1);
e_rdc2 = generateRDCs(vecs,EstTensor2);
delta_rdc1 = generateRDCs(vecs,Tensor1 - EstTensor1);
delta_rdc2 = generateRDCs(vecs,Tensor2 - EstTensor2);
delta_rdc1_1 = rdc1 - e_rdc1;
delta_rdc2_1 = rdc2 - e_rdc2;
scoreRDC1 = 0;
scoreRDC2 = 0;
for i=1: size(vecs,1)
    scoreRDC1 = scoreRDC1 + (delta_rdc1(i) - delta_rdc1_1(i));
    scoreRDC2 = scoreRDC2 + (delta_rdc2(i) - delta_rdc2_1(i));
end
end