function [vecs] = generateVectors(n)
% generate n vectors with magnitude 1
vecs = zeros(n,3);
for i=1:size(vecs)
    a = rand()*2 -1;
    b = rand()*2 -1;
    c = rand()*2 -1;
    mag = sqrt(a^2 + b^2 + c^2);
    a = a/mag;
    b = b/mag;
    c = c/mag;
    vecs(i,1) = a;
    vecs(i,2) = b;
    vecs(i,3) = c;
end
end