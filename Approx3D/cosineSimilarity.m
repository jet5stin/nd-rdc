function [Sim] = cosineSimilarity(v,w)
%must be same size, n x 3
n = size(v,1);
Sim = zeros(n,1);
    for i=1:n
       Sim(i,1) = dot(v(i,:),w(i,:))/(norm(v(i,:))*norm(w(i,:)));
    end
end