function [rdc] = generateRDCs(vecs,tensor)
rdc = zeros(size(vecs,1),1);
for i=1:size(rdc)
    rdc(i) = vecs(i,:)*tensor*vecs(i,:)';
end
end