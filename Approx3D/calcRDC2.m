function RDC=calcRDC2(v,S)
% function calcRDC(v,S) returns the RDCs for the
% vectors in the n by 3 matrix v.  

mags=sqrt(sum(v.^2,2));
v=v./[mags mags mags];
RDC=zeros(1,size(v,1));
for i=1:size(v,1)
    RDC(i)=v(i,:)*S*v(i,:)';
end
