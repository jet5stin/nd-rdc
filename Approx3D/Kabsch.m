function [U] = Kabsch (input1, input2)

cinput1 = input1 - mean(input1);
cinput2 = input2 - mean(input2);

A = cinput1' * cinput2;
[v,s,w] = svd(A);
U = w*[1 0 0; 0 1 0; 0 0 sign(det(w*v'))]*v';

end
