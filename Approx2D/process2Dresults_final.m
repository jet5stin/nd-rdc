function [S1,S2]=process2Dresults5(S1in,S2in)
    %initialize cell arrays and set first solution as the first matrix in
    %S1 and S2
    ClusteredSolutionS1={S1in{1}};
    ClusteredSolutionS2={S2in{1}};
    
    %initialize frequency tracker
    frequencyS1 = [1];
    frequencyS2 = [1];
    
    %initialize allocation tracker
    allocationS1 = [1 0 0 0 0 0 0 0];
    allocationS2 = [1 0 0 0 0 0 0 0];
    
    %for loop to cycle through S1in and S2in
    for i=2: length(S1in)
        %initialize minimum m score
        minScoreS1 = 1000;
        minScoreS2 = 1000;
        %initialize minimum index
        minIndexS1 = 0;
        minIndexS2 = 0;
        %for loop to compare next input from S1 to all clustered S1 sol.
        for j=1:length(ClusteredSolutionS1)
            mscoreS1 = otmdist(S1in{i},ClusteredSolutionS1{1,j})*23634;
            if mscoreS1 < minScoreS1 %compare scores
                minScoreS1 = mscoreS1;
                minIndexS1 = j; % keeps track of which cluster it best applies to
            end
        end
        
        %now see if the matrix matched any of the clustered solutions
        disp('Iteration')
        disp(i)
        disp('Min S1')
        disp(minScoreS1)
        if minScoreS1 < 1
            %matched a solution. update frequency and allocation
            allocationS1(i) = minIndexS1;
            frequencyS1(minIndexS1) = frequencyS1(minIndexS1) + 1;
            
        else
            %didn't match a solution, create a new cluster.
            ClusteredSolutionS1{length(ClusteredSolutionS1)+1} = S1in{i};
            %update frequency and allocation
            frequencyS1(length(frequencyS1)+1) = 1;
            allocationS1(i) = length(frequencyS1);
        end
        
        %for loop to compare next input from S2 to all clustered S2 sol
        for j=1:length(ClusteredSolutionS2)
            mscoreS2 = otmdist(S2in{i},ClusteredSolutionS2{1,j})*23634;
            if mscoreS2 < minScoreS2 %compare scores
                minScoreS2 = mscoreS2;
                minIndexS2 = j; % keeps track of which cluster it best applies to
            end
        end
        disp('Min S2')
        disp(minScoreS2)
        %now see if the matrix matched any of the clustered solutions
        if minScoreS2 < 1
            %matched a solution. update frequency and allocation
            allocationS2(i) = minIndexS2;
            frequencyS2(minIndexS2) = frequencyS2(minIndexS2) + 1;
        else
            %didn't match a solution, create a new cluster.
            ClusteredSolutionS2{length(ClusteredSolutionS2)+1} = S2in{i};
            %update frequency and allocation
            frequencyS2(length(frequencyS2)+1) = 1;
            allocationS2(i) = length(frequencyS2);
        end
    end
    
    %create solution web
    ClusterScoreS1 = [0 0 0 0 0 0 0 0];
    for i=1:length(ClusteredSolutionS1)
        for j=1:length(S1in)
            for k=1:length(S1in)
                if allocationS1(j) == i && allocationS1(k) == i && (j ~= k || frequencyS1(i) == 1)
                    ClusterScoreS1(j) = ClusterScoreS1(j) + otmdist(S1in{j},S1in{k})*23634;
                end
            end
        end
    end
    
    %create solution web
    ClusterScoreS2 = [0 0 0 0 0 0 0 0];
    for i=1:length(ClusteredSolutionS2)
        for j=1:length(S2in)
            for k=1:length(S2in)
                if allocationS2(j) == i && allocationS2(k) == i && (j ~= k || frequencyS2(i) == 1)
                    ClusterScoreS2(j) = ClusterScoreS2(j) + otmdist(S2in{j},S2in{k})*23634;
                end
            end
        end
    end
    
    S1 = {};
    %done comparing, compose final solutions
    for i=1:length(ClusteredSolutionS1)
        S1{i} = zeros(3,3);
        minIndexS1 = 0;
        minScoreS1 = 1000;
        for j = 1: length(S1in)
            if allocationS1(j) == i
                if ClusterScoreS1(j) < minScoreS1
                    minScoreS1 = ClusterScoreS1(j);
                    disp('found better solution')
                    disp(minIndexS1)
                    disp(j)
                    minIndexS1 = j;
                end
            end
        end
        S1{i} = S1in{minIndexS1};
    end

    for i=1:length(ClusteredSolutionS2)
        S2{i} = zeros(3,3);
        minIndexS2 = 0;
        minScoreS2 = 1000;
        for j = 1: length(S2in)
            if allocationS2(j) == i
                if ClusterScoreS2(j) < minScoreS2
                    minScoreS2 = ClusterScoreS2(j);
                    disp('found better solution')
                    disp(minIndexS2)
                    disp(j)
                    minIndexS2 = j;
                end
            end
        end
        S2{i} = S2in{minIndexS2};
    end
end