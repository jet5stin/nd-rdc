function v=randUnitSphere(n)
% function randUnitSphere(n) returns an n by 3 matrix of vectors randomly
% distributed about the surface of the unit sphere

v=randn([n 3]);
lens=sqrt(sum(v.*v,2));
v=v./[lens lens lens];
